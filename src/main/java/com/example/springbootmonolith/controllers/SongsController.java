package com.example.springbootmonolith.controllers;



import com.example.springbootmonolith.models.Song;
import com.example.springbootmonolith.models.User;
import com.example.springbootmonolith.repositories.SongRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class SongsController {

    @Autowired
    private SongRepository songRepository;

    @GetMapping("/songs")
    public Iterable<Song> findAllSongs() {
        return songRepository.findAll();
    }

    @GetMapping("/songs/{songId}")
    public Optional<Song> findSongById(@PathVariable Long songId) {
        return songRepository.findById(songId);
    }

    @DeleteMapping("/songs/{songId}")
    public HttpStatus deleteSongById(@PathVariable Long songId) {
        songRepository.deleteById(songId);
        return HttpStatus.OK;
    }

    @PostMapping("/songs")
    public Song createnewSong(@RequestBody Song newSong) {
        return songRepository.save(newSong);
    }

    @PatchMapping("/users/{userId}")
    public Song updateUserById(@PathVariable Long songID, @RequestBody Song songRequest) {

        Song songFromDb = songRepository.findById(songID).get();

        songFromDb.title=songRequest.title;
        songFromDb.length=songRequest.length;

        return songRepository.save(songFromDb);
    }

}
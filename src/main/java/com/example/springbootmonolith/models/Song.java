package com.example.springbootmonolith.models;

import lombok.*;
import javax.persistence.*;

@Data
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
@Entity @Table(name = "SONGS")
public class Song {

//    public User(String userName, String firstName, String lastName) {
//        this.userName = userName;
//        this.firstName = firstName;
//        this.lastName = lastName;
//    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "TITLE")
    public String title;

    @Column(name = "LENGTH")
    public String length;


}